FROM maven:3-jdk-8-alpine as builder

WORKDIR /opt/app
COPY pom.xml ./
COPY ./src ./src
RUN mvn clean install


FROM maven:3-jdk-8-alpine

WORKDIR /opt/app
EXPOSE 5000
COPY --from=builder /opt/app/target/*.jar /opt/app/*.jar
ENTRYPOINT ["java", "-jar", "/opt/app/*.jar" ]
